//
//  CalculatorBrain.swift
//  SuperCalculator
//
//  Created by Benedek Varga on 2017. 04. 20..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import Foundation

func randomDoubleBetweenZeroAndOne() -> Double {
    return Double(Int(arc4random_uniform(1000000)))/1000000
}

struct CalculatorBrain{
    
    mutating func addUnaryOperation(named symbol: String, _ operation: @escaping (Double)->Double)  {
        operations[symbol] = Operation.unaryOperation(operation)
    }
    
    
    
    private var accumulator: Double?
    private var pendingDesciption: String?
    private static var modeIsInDegNotRad = true
    private var resultIsPending = true
    
    private enum Operation{
        case constant(Double)
        case unaryOperation((Double)->Double)
        case binaryOperation((Double, Double)->Double)
        case equals
        case generateRandomNumber(()->Double)
        case resetEverything
    }
    
    private var operations: Dictionary<String, Operation> = [
        "√" : Operation.unaryOperation(sqrt),
        "π" : Operation.constant(Double.pi),
        "e" : Operation.constant(M_E),
        "±" : Operation.unaryOperation({-$0}),
        "sin" : Operation.unaryOperation({(number: Double) -> Double in
            return modeIsInDegNotRad ? sin(number*Double.pi/180.0) : sin(number)}),
        "cos" : Operation.unaryOperation({(number: Double) -> Double in
            return modeIsInDegNotRad ? cos(number*Double.pi/180.0) : cos(number)}),
        "tan" : Operation.unaryOperation({(number: Double) -> Double in
            return modeIsInDegNotRad ? tan(number*Double.pi/180.0) : tan(number)}),
        "log" : Operation.unaryOperation(log),
        "x" : Operation.binaryOperation({$0 * $1}),
        "÷" : Operation.binaryOperation({$0 / $1}),
        "+" : Operation.binaryOperation({$0 + $1}),
        "-" : Operation.binaryOperation({$0 - $1}),
        "=" : Operation.equals,
        "🤘🏻" : Operation.generateRandomNumber(randomDoubleBetweenZeroAndOne),
        "C" : Operation.resetEverything
    ]
    
    mutating func performOperation(_ symbol: String){
        if let operation = operations[symbol]{
            switch operation {
            case .constant(let value):
                accumulator = value
                pendingDesciption = pendingDesciption! + symbol
                //resultIsPending = false
            case .unaryOperation(let function):
                if accumulator != nil{
                    pendingDesciption = symbol + "(" + pendingDesciption! + ")"
                    accumulator = function(accumulator!)
                }
            case .binaryOperation(let function):
                if accumulator != nil{
                    pendingDesciption = pendingDesciption! + symbol
                    pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                    accumulator = nil
                }
            case .equals:
                performPendingBinaryOperation()
                resultIsPending = false
            case .generateRandomNumber(let value):
                accumulator = value()
                pendingDesciption = "rnjesus"
                resultIsPending = false
            case .resetEverything:
                accumulator = 0
                pendingDesciption = ""
                CalculatorBrain.modeIsInDegNotRad = true
                resultIsPending = true
                pendingBinaryOperation = nil
            }
        }
    }
    
    mutating private func performPendingBinaryOperation(){
        if pendingBinaryOperation != nil, accumulator != nil{
            accumulator = pendingBinaryOperation!.perform(with: accumulator!)
            pendingBinaryOperation = nil
        }
    }
    
    private var pendingBinaryOperation: PendingBinaryOperation?
    
    private struct PendingBinaryOperation{
        let function: (Double, Double)->Double
        let firstOperand: Double
        
        func perform(with secondOperand: Double) -> Double {
            return function(firstOperand, secondOperand)
        }
    }
    
    mutating func setOperand(_ operand: Double) {
        accumulator = operand
        

        if pendingDesciption != nil{
            pendingDesciption = pendingDesciption! + String(format: "%g", accumulator!)
        }
        else{
            pendingDesciption = String(format: "%g", accumulator!)
        }
    }
    
    mutating func setRadDegSwitch(to flag: Bool){
        CalculatorBrain.modeIsInDegNotRad = flag
    }
    
    var result: Double?{
        get{
            return accumulator
        }
    }
    
    var description: String?{
        get{
            return pendingDesciption
        }
    }
    
    var resultIsPendingState: Bool?{
        get{
            return resultIsPending
        }
    }
    
    var calculatorModeIsDEG: Bool?{
        get{
            return CalculatorBrain.modeIsInDegNotRad
        }
    }
}
