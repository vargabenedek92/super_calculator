//
//  ViewController.swift
//  SuperCalculator
//
//  Created by Benedek Varga on 2017. 04. 20..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //properties
    @IBOutlet weak var display: UILabel!{
        didSet{
            display.isUserInteractionEnabled = true
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(deleteBySwipe))
            swipeLeft.direction = .left
            display.addGestureRecognizer(swipeLeft)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        brain.addUnaryOperation(named: "✅"){ [weak weakSelf = self] in
            weakSelf?.display.textColor = UIColor.green
            return sqrt($0)
        }
    }
    
    var userIsInTheMiddleOfTyping = false
    var calculatorIsInRadMode = false
    
    @IBOutlet weak var decriptionDisplay: UILabel!
    
    //computed property example
    var displayValue: Double{
        get{
            return Double(display.text!)!
        }
        set{
            let formatter = NumberFormatter()
            formatter.maximumFractionDigits = 6
            formatter.minimumIntegerDigits = 1
            let formattedString = formatter.string(from: NSNumber(value: newValue))
            display.text = formattedString
        }
    }
    @IBAction func pressButton(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)})
    }
    
    //methods
    @IBAction func touchDigit(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform.identity})
        
        let digit = sender.currentTitle!
        
        if digit == ".", display.text!.contains("."){
            display.text = String("Nope!🖕🏻")
            userIsInTheMiddleOfTyping = false
            return
        }
        
        if userIsInTheMiddleOfTyping{
            if digit == "DEL", display != nil, display.text!.characters.count != 1{
                display.text = display.text!.substring(to: display.text!.index(before: display.text!.endIndex))
            }else{
                if digit == "DEL"{
                    display.text! = "0"
                }
                else{
                    let textCurrentlyInDisplay = display.text!
                    display.text = textCurrentlyInDisplay + digit
                }
            }
        }
        else{
            if digit == "DEL"{
                return
            }
            display.text = digit
            userIsInTheMiddleOfTyping = true
        }
    }
    
    @IBOutlet weak var radDegreeSwitchButton: UIButton!
    
    private var brain = CalculatorBrain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform.identity})
        
        if userIsInTheMiddleOfTyping{
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        
        if let mathematicalSymbol = sender.currentTitle{
            brain.performOperation(mathematicalSymbol)
        }
        
        if let result = brain.result{
            displayValue = result
        }
        
        if sender.currentTitle == "RAD" || sender.currentTitle == "DEG"{
            if let btnTitle = sender.currentTitle{
                if btnTitle == "DEG"{
                    sender.setTitle("RAD", for: .normal)
                    brain.setRadDegSwitch(to: false)
                }
                else{
                    sender.setTitle("DEG", for: .normal)
                    brain.setRadDegSwitch(to: true)
                }
            }
        }
        
        if brain.resultIsPendingState!{
            decriptionDisplay.text = brain.description! + "..."
        }
        else{
            decriptionDisplay.text = brain.description! + "="
        }
        
    }
    
    func deleteBySwipe() {
        if userIsInTheMiddleOfTyping{
            if display != nil, display.text!.characters.count != 1{
                display.text = display.text!.substring(to: display.text!.index(before: display.text!.endIndex))
            }else{
                display.text! = "0"
            }
            
        }
    }
    
    
}
